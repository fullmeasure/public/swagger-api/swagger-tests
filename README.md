#swagger-tests
Specification driven REST api development written in typescript.

## Installation
```yarn add swagger-tests```

## Usage
```npm version patch -m "Version %s - add sweet badges"```

## Debug
sym link into node modules and change package.json to
  "main": "lib/index.ts",
and remove types