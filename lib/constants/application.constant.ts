import { API } from "./index";

export class Application {
  public static serverUrl(): string {
    return process.env.SERVER_BASE_URL || API.servers[0].url
  }
}
