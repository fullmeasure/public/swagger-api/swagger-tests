import { SwaggerResponse } from "./response";

export class SwaggerOperationResponses {
  public 200: SwaggerResponse;
  public 404: SwaggerResponse;
  public 422: SwaggerResponse;
  public 401: SwaggerResponse;
  public 500: SwaggerResponse;
}
