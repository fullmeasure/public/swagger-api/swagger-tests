import { SwaggerParameter } from "./parameter";
import { SwaggerOperationResponses } from "./operation_responses";

export class SwaggerOperation {
  public summary: string;
  public description: string;
  public parameters: SwaggerParameter[];
  public responses: SwaggerOperationResponses;
}
