import { SwaggerParameterSchema } from "./parameter_schema";

export class SwaggerParameter {
  public name: string;
  public in: string;
  public required: boolean;
  public description: string;
  public schema: SwaggerParameterSchema;
}
