import { SwaggerComponentResponses } from "./component_responses";
import { SwaggerSecurityScheme } from "./security_scheme";

export class SwaggerComponents {
  public responses: SwaggerComponentResponses;
  public schemas: any;
  public requestBodies: any;
  public securitySchemes: SwaggerSecurityScheme;
}
