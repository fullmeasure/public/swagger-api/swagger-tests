export class SwaggerSecurityScheme {
  public type: string;
  public name: string;
  public in: string;
}
