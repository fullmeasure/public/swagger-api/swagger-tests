import { SwaggerResponse } from "./response";

export class SwaggerComponentResponses {
  public NotFound: SwaggerResponse;
  public Unauthorized: SwaggerResponse;
  public BadRequest: SwaggerResponse;
  public Unexpected: SwaggerResponse;
}