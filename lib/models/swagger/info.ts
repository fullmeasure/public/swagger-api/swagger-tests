export class SwaggerInfo {
  public version: string;
  public title: string;
  public description: string;
}
