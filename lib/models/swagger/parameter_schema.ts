export class SwaggerParameterSchema {
  public type: string;
  public format: string;
  public minimum: number;
}
