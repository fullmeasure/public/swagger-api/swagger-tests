import { SwaggerResponseContentOptions } from "./response_content_options";

export class SwaggerResponseContent {
  public "application/json; charset=utf-8": SwaggerResponseContentOptions
}
