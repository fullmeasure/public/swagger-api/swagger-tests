import { SwaggerResponseContentOptionsSchema } from "./response_content_options_schema";

export class SwaggerResponseContentOptions {
  public schema: SwaggerResponseContentOptionsSchema;
}
