import { SwaggerSecurityScheme } from "./security_scheme";

export class SwaggerSecuritySchemes {
  public api_key: SwaggerSecurityScheme;
}
