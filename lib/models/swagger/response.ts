import { SwaggerResponseContent } from "./response_content";

export class SwaggerResponse {
  public description: string;
  public content: SwaggerResponseContent;
  public required: boolean;
}
