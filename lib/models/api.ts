import { SwaggerComponents } from "./swagger/components";
import { SwaggerSecurity } from "./swagger/security";
import { SwaggerInfo } from "./swagger/info";
import { SwaggerServer } from "./swagger/server";
import { extend } from 'underscore';
import { readFileSync } from 'fs';

export class SwaggerApi {
  public openapi: string;
  public security: SwaggerSecurity[];
  public info: SwaggerInfo;
  public servers: SwaggerServer[];
  public paths: any;
  public components: SwaggerComponents;

  private testedOperations: string[] = [];

  constructor() {
    this.create(extend(this, this.json()));
  }

  public create(api: SwaggerApi): SwaggerApi {
    api.components = extend(new SwaggerComponents(), api.components);
    api.info = extend(new SwaggerInfo(), api.info);
    api.servers = api.servers.map((server) => {
      return extend(new SwaggerServer(), server);
    });
    return api;
  }

  public json(): any {
    return JSON.parse(readFileSync('api.json', 'utf8'));
  }

  public routes(): string[] {
    return Object.keys(this.paths);
  }

  public routeOperations(route: string): string[] {
    return Object.keys(this.paths[route])
  }

  public routeOperationResponses(route: string, operation: string): string[] {
    return Object.keys(this.paths[route][operation].responses);
  }

  public operationResponses(): string[] {
    let ids: string[] = [];
    this.routes().map((route: string) => {
      return this.routeOperations(route).map((operation: string) => {
        this.routeOperationResponses(route, operation).map((statusCode) => {
          ids.push(`${route}#${operation}::${statusCode}`);
        });
      });
    });
    return ids;
  }

  public setTestedOperation(operation: string): void {
    this.testedOperations.push(operation);
  }

  public getTestedOperations(): string[] {
    return this.testedOperations;
  }
}
