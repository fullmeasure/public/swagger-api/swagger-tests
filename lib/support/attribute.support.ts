import { pick, map, last } from "underscore";
import { API } from "../constants";
import { SwaggerParameterSchema } from "../models/swagger/parameter_schema";
import { endsWith, underscored } from "underscore.string";
import { name, random, date, lorem, internet, finance } from "faker";
import { ApiSupport } from './api.support';

export class AttributeSupport {

  constructor(protected apiSupport: ApiSupport) {}

  public async valid(): Promise<any> {
    return pick(this.faker(), this.required());
  }

  public async invalid(): Promise<any> {
    let body = this.faker();
    body['invalid_parameter'] = lorem.words(10);
    return body;
  }

  public schema(): any {
    const schema = API.components.schemas[this.apiSupport.modelName];
    return schema || this.sti_schema();
  }

  public sti_schema(): any {
    const schemaName: string = last(API.paths[`/${underscored(this.apiSupport.modelName)}s/{id}/`].get.responses['200'].content['application/json; charset=utf-8'].schema['oneOf'][0]['$ref'].split('/')) as string;
    return API.components.schemas[schemaName];
  }

  public required(): string[] {
    return this.schema().required;
  }

  protected all(): string[] {
    return this.schema().properties;
  }

  protected faker(): any {
    let fake = {};
    map(this.all(), (schema: SwaggerParameterSchema, attribute: string) => {
      if(endsWith(attribute, '_id') && schema.type == 'string') {
        fake[attribute] = random.uuid();
      }
      else if(attribute == 'first_name') {
        fake[attribute] = name.firstName();
      }
      else if(attribute == 'last_name') {
        fake[attribute] = name.lastName();
      }
      else if(attribute == 'email') {
        fake[attribute] = internet.exampleEmail();
      }
      else if(schema.type == 'string' && (schema.format === 'string' || schema.format === undefined)) {
        fake[attribute] = lorem.words(10);
      }
      else if(schema.type == 'integer') {
        fake[attribute] = random.number({min: 1, max: 999999});
      }
      else if(schema.type == 'integer') {
        fake[attribute] = random.number({min: 1, max: 999999});
      }
      else if(schema.type == 'number') {
        fake[attribute] = finance.amount(1, 999999, 3);
      }
      else if(schema.format == 'date-time') {
        fake[attribute] = new Date();
      }
      else if (schema.format == 'date') {
        fake[attribute] = date.recent(10);
      }
      else{
        fake[attribute] = lorem.words(10);
      }
      return fake[attribute];
    });
    return fake;
  }
}
