import {} from 'jasmine';
import { contains } from 'underscore';

const actionMapping = {
  post: 'create',
  put: 'update',
  get: 'index',
  delete: 'delete',
};

const statusMapping = {
  404: 'notFound',
  422: 'bad',
  500: 'unexpected',
  401: 'unauthorized',
};

export class OperationSupport {
  public readonly restMethod: string;
  public readonly statusCode: string;
  public readonly route: string;

  public constructor(operationName: string) {
    const routeSplit = operationName.split('#');
    this.route = routeSplit[0];
    this.restMethod = routeSplit[1].split('::')[0];
    this.statusCode = routeSplit[1].split('::')[1];
  }

  public get restAction(): string {
    if (this.restMethod == 'get' && contains(this.route, '{id}')) {
      return 'show';
    } else {
      return actionMapping[this.restMethod];
    }
  }

  public get statusString(): string {
    let status: string = statusMapping[this.statusCode];
    if (this.statusCodeInt >= 200 && this.statusCodeInt < 400) {
      status = 'success'
    }
    return status;
  }

  private get statusCodeInt(): number {
    return parseInt(this.statusCode, 10);
  }
}
