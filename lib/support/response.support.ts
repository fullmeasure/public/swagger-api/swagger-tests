import {} from 'jasmine'
import { AxiosResponse } from "axios";
import { first, intersection, contains } from 'underscore';
import { ApiSupport } from './api.support';

export class ResponseSupport {
  public constructor(protected apiSupport: ApiSupport) { }

  public async success(response: AxiosResponse): Promise<any> {
    expect(response.status.toString()).toEqual(this.apiSupport.operation.statusCode);
    if(this.apiSupport.operation.restAction === 'index') {
      response.data = first(response.data);
    }
    if (contains(['index', 'show'], this.apiSupport.operation.restAction)) {
      expect(this.apiSupport.attributes.required()).toEqual(intersection(this.apiSupport.attributes.required(), Object.keys(response.data)));
    }
    if (contains(['create', 'update'], this.apiSupport.operation.restAction) && response.status >= 300 && response.status < 400) {
      expect(response.headers.location).toBeDefined('location missing from headers');
    }
    return true;
  }

  public async notFound(response: AxiosResponse): Promise<any> {
    expect(response.status.toString()).toEqual(this.apiSupport.operation.statusCode);
  }

  public async bad(response: AxiosResponse): Promise<any> {
    expect(response.status.toString()).toEqual(this.apiSupport.operation.statusCode);
  }

  public async unauthorized(response: AxiosResponse): Promise<any> {
    expect(response.status.toString()).toEqual(this.apiSupport.operation.statusCode);
  }
}