import { HttpService } from '../services';
import { contains } from 'underscore';
import { ApiSupport } from './api.support';

export class BeforeSupport {
  public constructor(public apiSupport: ApiSupport) { }

  public async success(): Promise<any> {
    await this.addAuthorization();
    if(contains(['delete', 'update', 'show', 'index'], this.apiSupport.operation.restAction)) {
      const validAttributes = await(this.apiSupport.attributes.valid());
      return HttpService.post({...this.apiSupport.axiosConfig, maxRedirects: 1}, this.apiSupport.route, validAttributes);
    } else {
      return true;
    }
  }

  public async bad(): Promise<any> {
    await this.addAuthorization();
    if(contains(['update'], this.apiSupport.operation.restAction)) {
      const validAttributes = await(this.apiSupport.attributes.valid());
      return HttpService.post({...this.apiSupport.axiosConfig, maxRedirects: 1}, this.apiSupport.route, validAttributes);
    } else {
      return true;
    }
  }

  public async notFound(): Promise<any> {
    await this.addAuthorization();
    return true;
  }

  public async unauthorized(): Promise<any> {
    this.removeAuthorization();
    return true;
  }

  protected removeAuthorization(): void {
    if (this.apiSupport.axiosConfig.headers !== undefined && this.apiSupport.axiosConfig.headers.Authorization !== undefined) {
      delete this.apiSupport.axiosConfig.headers.Authorization;
    }
  }

  protected async addAuthorization(): Promise<any> {
    return true;
  }
}
