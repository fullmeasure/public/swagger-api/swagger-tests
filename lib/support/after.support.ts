import { AxiosResponse } from "axios";
import { HttpService } from "../services";
import { ApiSupport } from './api.support';
import { contains } from 'underscore';

export class AfterSupport {
  public constructor(protected apiSupport: ApiSupport) { }

  public async success(response: AxiosResponse): Promise<any> {
    await this.addAuthorization();
    if(contains(['delete', 'create'], this.apiSupport.operation.restAction)) {
      return true;
    } else {
      return HttpService.delete(this.apiSupport.axiosConfig, this.apiSupport.route, {}, response.data.id);
    }
  }

  public async bad(_response: AxiosResponse): Promise<any> {
    return true;
  }

  public async notFound(_response: AxiosResponse): Promise<any> {
    return this.apiSupport.operation.restMethod;
  }

  public async unauthorized(_response: AxiosResponse): Promise<any> {
    return this.apiSupport.operation.restMethod;
  }

  protected async addAuthorization(): Promise<any> {
    return true;
  }
}
