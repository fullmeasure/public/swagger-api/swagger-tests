import { HttpService } from "../services";
import { contains } from 'underscore';
import { ApiSupport } from './api.support';

export class RequestSupport {
  public constructor(protected apiSupport: ApiSupport) { }

  public async success(response: any): Promise<any> {
    await this.addAuthorization();
    if(this.apiSupport.operation.restAction == 'create') {
      return HttpService[this.apiSupport.operation.restMethod](this.apiSupport.axiosConfig, this.apiSupport.route, await(this.apiSupport.attributes.valid()));
    } else if(this.apiSupport.operation.restAction == 'update') {
      return HttpService[this.apiSupport.operation.restMethod](this.apiSupport.axiosConfig, this.apiSupport.route, await(this.apiSupport.attributes.valid()), response.data.id);
    } else if(contains(['show', 'delete'], this.apiSupport.operation.restAction)) {
      return HttpService[this.apiSupport.operation.restMethod](this.apiSupport.axiosConfig, this.apiSupport.route, {}, response.data.id);
    } else {
      return HttpService.query(this.apiSupport.axiosConfig, this.apiSupport.route, {});
    }
  }

  public async bad(response: any): Promise<any> {
    await this.addAuthorization();
    if(this.apiSupport.operation.restAction == 'create') {
      return HttpService[this.apiSupport.operation.restMethod](this.apiSupport.axiosConfig, this.apiSupport.route, await(this.apiSupport.attributes.invalid()));
    } else if(this.apiSupport.operation.restAction == 'update') {
      return HttpService[this.apiSupport.operation.restMethod](this.apiSupport.axiosConfig, this.apiSupport.route, await(this.apiSupport.attributes.invalid()), response.data.id);
    } else  if(this.apiSupport.operation.restAction == 'index') {
      return HttpService.query(this.apiSupport.axiosConfig, this.apiSupport.route, await(this.apiSupport.attributes.invalid()));
    } else {
      return HttpService[this.apiSupport.operation.restMethod](this.apiSupport.axiosConfig, this.apiSupport.route, await(this.apiSupport.attributes.invalid()), response.data.id);
    }
  }

  public async notFound(_response: any): Promise<any> {
    await this.addAuthorization();
    if(this.apiSupport.operation.restAction == 'update') {
      return HttpService[this.apiSupport.operation.restMethod](this.apiSupport.axiosConfig, this.apiSupport.route, await(this.apiSupport.attributes.valid()), -999);
    } else {
      return HttpService[this.apiSupport.operation.restMethod](this.apiSupport.axiosConfig, this.apiSupport.route, {}, -999);
    }
  }

  public async unauthorized(_response: any): Promise<any> {
    this.removeAuthorization();
    if (this.apiSupport.operation.restAction == 'index'){
      return HttpService.query(this.apiSupport.axiosConfig, this.apiSupport.route, {});
    } else if (this.apiSupport.operation.restAction == 'create'){
      return HttpService[this.apiSupport.operation.restMethod](this.apiSupport.axiosConfig, this.apiSupport.route, await(this.apiSupport.attributes.valid()));
    } else {
      return HttpService[this.apiSupport.operation.restMethod](this.apiSupport.axiosConfig, this.apiSupport.route, {}, -999);
    }
  }

  protected removeAuthorization(): void {
    if (this.apiSupport.axiosConfig.headers !== undefined && this.apiSupport.axiosConfig.headers.Authorization !== undefined) {
      delete this.apiSupport.axiosConfig.headers.Authorization;
    }
  }

  protected async addAuthorization(): Promise<any> {
    return true;
  }
}
