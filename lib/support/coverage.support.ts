import {} from 'jasmine';
import { API } from "../constants/";
import { contains } from 'underscore';

export class CoverageSupport {
  constructor(private operationStr: string) { }

  public verify(): boolean {
    if (contains(API.operationResponses(), this.operationStr)) {
      API.setTestedOperation(this.operationStr);
      return false;
    } else {
      pending(`There is no swagger defined version of ${this.operationStr}.`);
      return true;
    }
  }
}
