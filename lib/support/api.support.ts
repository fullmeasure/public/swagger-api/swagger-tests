import { AttributeSupport } from "./attribute.support";
import { OperationSupport } from './operation.support';
import { RequestSupport } from "./request.support";
import { ResponseSupport } from "./response.support";
import { BeforeSupport } from "./before.support";
import { AfterSupport } from "./after.support";
import { AxiosRequestConfig } from 'axios';

export abstract class ApiSupport {
  protected constructor(operationStr: string, protected debug: boolean = false) {
    this.operation = new OperationSupport(operationStr);
  }

  public readonly modelName: string = this.constructor.name.toString().replace('Api', '');
  public readonly attributes: AttributeSupport = new AttributeSupport(this);
  public readonly operation: OperationSupport;
  public axiosConfig: AxiosRequestConfig = {};

  protected before: BeforeSupport = new BeforeSupport(this);
  protected after: AfterSupport = new AfterSupport(this);
  protected request: RequestSupport = new RequestSupport(this);
  protected response: ResponseSupport = new ResponseSupport(this);

  public get route(): string {
    return this.operation.route.replace('{id}/', '');
  }

  public async test(): Promise<any> {
    this.step('before');
    const beforeHookResponse = await (this.before[this.operation.statusString]());
    this.step(beforeHookResponse);
    this.step('request');
    const requestHookResponse = await (this.request[this.operation.statusString](beforeHookResponse));
    this.step(`after received: ${requestHookResponse.status.toString()}`);
    this.step(requestHookResponse);
    this.step('response');
    await this.response[this.operation.statusString](requestHookResponse);
    this.step('after');
    const afterResponse = await this.after[this.operation.statusString](beforeHookResponse);
    this.step(afterResponse);
  }

  public step(name: any): void {
    if(this.debug) {
      console.log(name, this.modelName, this.operation.restAction, this.operation.statusString);
    }
  }
}
