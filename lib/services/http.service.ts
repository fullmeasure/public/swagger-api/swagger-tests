import { Application } from "../constants/application.constant";
import axios, { AxiosRequestConfig } from 'axios';

const defaultConfig = {
  baseURL: Application.serverUrl(),
  maxRedirects: 0,
  validateStatus: (_status: number) => true
};

export class HttpService {
  public static get(config: AxiosRequestConfig = {}, path: string, params: any = {}, id: string|number): Promise<any> {
    return axios({
      ...defaultConfig,
      ...config,
      method: 'get',
      url: `${path}${id}/`,
      params: params
    });
  }

  public static query(config: AxiosRequestConfig = {}, path: string, params: any = {}): Promise<any> {
    return axios({
      ...defaultConfig,
      ...config,
      method: 'get',
      url: path,
      params: params
    });
  }

  public static post(config: AxiosRequestConfig = {}, path: string, params: any): Promise<any> {
    return axios({
      ...defaultConfig,
      ...config,
      method: 'post',
      url: path,
      data: params
    });
  }

  public static put(config: AxiosRequestConfig = {}, path: string, params: any, id: string|number): Promise<any> {
    return axios({
      ...defaultConfig,
      ...config,
      method: 'put',
      url: `${path}${id}/`,
      data: params
    });
  }

  public static delete(config: AxiosRequestConfig = {}, path: string, params: any = {}, id: string|number): Promise<any> {
    return axios({
      ...defaultConfig,
      ...config,
      method: 'delete',
      url: `${path}${id}/`,
      params: params
    });
  }
}
