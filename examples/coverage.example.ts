import { difference } from 'underscore';
import { API } from "swagger-tests";

afterAll(() => {
  console.log(`coverage: ${Math.floor((API.getTestedOperations().length/API.operationResponses().length) * 100)}% - expected: 100%`);
  expect(difference(API.operationResponses(), API.getTestedOperations())).toEqual([]);
});
