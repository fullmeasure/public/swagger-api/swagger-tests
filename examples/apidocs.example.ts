import {} from 'jasmine'
import { API, SwaggerApi } from "swagger-tests";

describe('SwaggerApi', () => {
  it("should fetch the apidocs", () => {
    expect(API).toBeDefined();
    expect(API).toEqual(jasmine.any(SwaggerApi));
  });

  describe('openapi', () => {
    it('should be defined', () => {
      expect(API.openapi).toEqual(jasmine.any(String));
    })
  });
});
