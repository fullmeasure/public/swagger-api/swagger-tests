import { each, filter } from 'underscore';
import { underscored } from 'underscore.string';
import { API, CoverageSupport } from "swagger-tests";
import '../support/micro_services/axios';

const DEBUG = false;

describe('api', () => {
  each([], (api: any) => {
    const relatedOperations = filter(API.operationResponses(), (operationStr: string) => {
      return new RegExp(`\/${underscored(new api(operationStr).modelName)}s?\/`).test(operationStr);
    });
    if (DEBUG) {
      console.log('relatedOperations', new api('f#f::f').modelName, relatedOperations.length, relatedOperations);
    }
    relatedOperations.forEach((operationStr: string) => {
      describe(`${api.name}: ${operationStr} request`, () => {
        it(`should return a response`, (done) => {
          new CoverageSupport(operationStr).verify();
          new api(operationStr, DEBUG).test().then(() => done(), (error: any) => done.fail(error));
        });
      });
    });
  });
});
